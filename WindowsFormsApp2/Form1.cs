﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        protected Graphics g;
        GFX engine;
        plc plc;

        public Form1()
        {

            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics u = panel1.CreateGraphics();
            engine = new GFX(u);

            plc = new plc();
            plc.initplc();

            osvjezi();

        }

        private void panel1_Click(object sender, EventArgs e)
        {
            Point mouse = Cursor.Position;
            mouse = panel1.PointToClient(mouse);

            plc.detectHit(mouse);
            osvjezi();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            plc.reset();
            GFX.setup();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void osvjezi()
        {
            label1.Text = plc.Count1.ToString();
            label3.Text = plc.Count2.ToString();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            label2.Text = Interaction.InputBox("", "PLAYER1 NAME", "", 200, 200) + ": ";
            label4.Text = Interaction.InputBox("", "PLAYER2 NAME", "", 200, 200) + ": ";
        }
    }
    class plc
    {

        private Holder[,] holders = new Holder[3, 3];
        public int potez = 0;
        private int count1 = 0;
        private int count2 = 0;


        public const int X = 0;
        public const int Y = 1;
        public const int B = 2;

        public int Count1
        {
            get
            {
                return count1;
            }
            set
            {
                count1 = value;
            }
        }
        public int Count2
        {
            get
            {
                return count2;
            }
            set
            {
                count2 = value;
            }
        }
        public void initplc()
        {

            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {

                    holders[x, y] = new Holder();
                    holders[x, y].Vrijednost = B;
                    holders[x, y].Lokacija = new Point(x, y);
                }
            }
        }
        public void detectHit(Point loc)
        {
            if (loc.Y < 392)
            {
                int x = 0;
                int y = 0;
                if (loc.X < 131)
                {
                    x = 0;
                }
                else if (loc.X > 131 && loc.X < 262)
                {
                    x = 1;
                }
                else
                {
                    x = 2;
                }
                if (loc.Y < 131)
                {
                    y = 0;
                }
                else if (loc.Y > 131 && loc.Y < 262)
                {
                    y = 1;
                }
                else if (loc.Y < 392)
                {
                    y = 2;
                }

                if (potez % 2 == 0)
                {
                    GFX.drawX(new Point(x, y));
                    holders[x, y].Vrijednost = X;
                    potez++;
                    if (Pobjeda(X))
                    {
                        count1++;
                        reset();
                        GFX.setup();


                    }
                }
                else
                {
                    GFX.drawO(new Point(x, y));
                    holders[x, y].Vrijednost = Y;
                    potez++;
                    if (Pobjeda(Y))
                    {
                        count2++;
                        reset();
                        GFX.setup();

                    }

                }

                if (potez == 9)
                {
                    reset();
                    GFX.setup();

                }

            }
        }
        public bool Pobjeda(int X)
        {
            bool win = false;
            int x;
            for (x = 0; x < 3; x++)
            {
                if (holders[x, 0].Vrijednost == X && holders[x, 1].Vrijednost == X && holders[x, 2].Vrijednost == X) return true;
                else if (holders[0, x].Vrijednost == X && holders[1, x].Vrijednost == X && holders[2, x].Vrijednost == X) return true;
            }
            x = 0;
            if (holders[x, x].Vrijednost == X && holders[x + 1, x + 1].Vrijednost == X && holders[x + 2, x + 2].Vrijednost == X) return true;
            if (holders[x, x + 2].Vrijednost == X && holders[x + 1, x + 1].Vrijednost == X && holders[x + 2, x].Vrijednost == X) return true;

            return win;
        }

        public void reset()
        {
            holders = new Holder[3, 3];
            initplc();
            potez = 0;
        }
    }



    class Holder
    {

        private Point lokacija;
        private int vrijednost = plc.B;
        public Point Lokacija
        {
            get
            {
                return lokacija;
            }
            set
            {
                lokacija = value;
            }
        }

        public int Vrijednost
        {
            get
            {
                return vrijednost;
            }
            set
            {
                vrijednost = value;
            }
        }

    }
    class GFX
    {
        private static Graphics gObj;
        public GFX(Graphics g)
        {
            gObj = g;
            setup();
        }
        public static void setup()
        {
            Brush bg = new SolidBrush(Color.WhiteSmoke);
            Pen p = new Pen(Color.Black);

            gObj.FillRectangle(bg, 0, 0, 392, 392);
            gObj.DrawLine(p, 131, 0, 131, 392);
            gObj.DrawLine(p, 262, 0, 262, 392);
            gObj.DrawLine(p, 0, 131, 392, 131);
            gObj.DrawLine(p, 0, 262, 392, 262);
        }
        public static void drawX(Point loc)
        {
            Pen pen = new Pen(Color.Blue, 1.5F);
            int xA = loc.X * 130;
            int yA = loc.Y * 130;
            gObj.DrawLine(pen, xA + 20, yA + 20, xA + 110, yA + 110);
            gObj.DrawLine(pen, xA + 110, yA + 20, xA + 20, yA + 110);
        }

        public static void drawO(Point loc)
        {
            Pen pen = new Pen(Color.YellowGreen, 1.5F);
            int xA = loc.X * 130;
            int yA = loc.Y * 130;
            gObj.DrawEllipse(pen, xA + 10, yA + 10, 110, 110);

        }
    }
}
